# Laboratory work 2
---
Practising usage of GET and POST queries with PHP

## Tasks
1. Make navigation menu and using GET requests make current active link active(highlighted).
2. Make form to send array to PHP using POST request. Add proccessing logic to filter duplicates and show processed array.


<?php
    // Enable error log
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    // Calculate current active list
    $current_id = 0;
    if (isset($_GET["id"]))
        $current_id = intval($_GET["id"]);

    // Generate menu
    $menu = array("List 1", "List 2", "List 3", "List 4", "List 5", "List 6");

    $has_post = isset($_POST["array"]);
    $data_valid = FALSE;
    if ($has_post) {
        // Get POST request
        $data_array_string = $_POST["array"];
        
        // Split array by comma
        $data_strings = preg_split("/\s*,\s*/", $data_array_string);
        
        // Check array for validity
        $validity_folder = function(bool $data_valid, string $item) : bool {
            return $data_valid && (filter_var($item, FILTER_VALIDATE_INT) !== FALSE);
        };
        $data_valid = array_reduce($data_strings, $validity_folder, TRUE);

        if ($data_valid) {
            // Convert all strings to integers
            $data_integers = array_map("intval", $data_strings);

            // Collect unique elements
            $data_uniques = array();
            foreach ($data_integers as $data_item)
                if (array_search($data_item, $data_uniques) === FALSE)
                    array_push($data_uniques, $data_item);
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Lab 2</title>
        <style>
            body, html {
                margin: 0;
                padding: 0;
                height: 100%;
            }
            .header {
                width: 100%;
                background-color: #aaa;
                display: flex;
                justify-content: center;
            }
            .menu {
                padding-top: 20px;
                padding-bottom: 20px;
            }

            .menu ul {
                list-style: none;
                display: flex;
                padding: 0;
                margin: 0;
            }
            .menu .menu-item a {
                display: block;
                background-color: #444;
                padding: 10px 30px;
                text-decoration: none;
                color: white;
                border: 2px solid transparent;
            }
            .menu .menu-item.active a {
                border: 2px solid lightgreen;
                background-color: #474;
            }

            .content {
                padding-top: 20px;
                background-color:lightblue;
                height: 100%;
            }
            .error {
                background-color: lightcoral;
                width: 20%;
                margin: auto;
                border: 2px solid darkred;
                height: 20%;
                display: flex;
                justify-content: center;
                align-items: center;
                margin-top: 20px;
            }
            .error p {
                padding: 0;
                margin: 0;
            }
            .form-container {
                display: flex;
                justify-content: center;
            }
            .data .title, 
            .processed-data .title {
                font-size: 2em;
                text-align: center;
                margin: 10px;
            }
            .data-list {
                display: flex;
                list-style: none;
                flex-wrap: wrap;
                flex-direction: row;
            }
            .data-list .data-item {
                margin: 10px;
                display: block;
                padding: 10px;
                background-color: lightgreen;
                color: darkgreen
            }
        </style>
    </head>
    <body>
        <header class="header">
            <nav class="menu">
                <ul>
                    <?php foreach($menu as $id => $menu_item) { ?>
                        <li class="menu-item <?= ($id == $current_id) ? "active" : "" ?>">
                            <a href="?id=<?= $id ?>"><?= $menu_item ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        </header>
        <main class="content">
            <div class="form-container">
                <form action="" method="post" class="array-form">
                    <label for="array">Array:</label>
                    <input type="text" name="array"></textarea>
                    <input type="submit" value="Send">
                </form>
            </div>
            <?php if ($data_valid && $has_post) { ?>
                <div class="data">
                    <h5 class="title">Input Array<h5>
                    <ul class="data-list">
                        <?php foreach ($data_integers as $data_item) { ?>
                            <li class="data-item">
                                <?= $data_item ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="processed-data">
                    <h5 class="title">Processed Array<h5>
                    <ul class="data-list">
                        <?php foreach ($data_uniques as $data_item) { ?>
                            <li class="data-item">
                                <?= $data_item ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } else if ($has_post) { ?>
                <div class="error">
                    <p>
                        Array data is invalid
                    </p>
                </div>
            <?php } ?>
        </main>
    </body>
</html>